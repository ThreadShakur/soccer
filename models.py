from jugo.models.Model import Model
import jugo.models.types as types


class TB1Model(Model):

    find_id = types.VarChar(max_length=128)
    check_id = types.Integer()
    url = types.MediumText()
    info_url = types.MediumText()
    teams = types.VarChar()
    score1 = types.VarChar()
    score2 = types.VarChar()
    league = types.VarChar()
    start = types.DateTime()
    h2h = types.Integer()
    total = types.Integer()
    total_this = types.Integer()
    cef = types.Float()
