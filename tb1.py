import requests
from bs4 import BeautifulSoup as bs
from datetime import datetime
import json
import random
import time
import logging
from jugo import manage
from models import TB1Model

manage.syncdb('models')
model = TB1Model()


class xbet():
    def __init__(self):
        self.session = requests.Session()
        self.champs = {}
        self.games = []


    def get_champs(self):
        response = self.session.get('https://1xstavka.ru/LineFeed/GetChampsZip?sport=1&lng=en&tf=2200000&tz=3&country=1&virtualSports=true').json()
        for item in response['Value']:
            self.champs.update({item['LI']: item['L'].replace('.', '')})


    def get_games(self, champ):
        result = []
        response = self.session.get(f'https://1xstavka.ru/LineFeed/GetChampZip?sport=1&champ={champ}&lng=en&tf=2200000&tz=3&country=1&virtualSports=true').json()
        games = response['Value']['G']
        for game in games:
            try:
                result.append({'id': game['CI'], 'left': game['O1'].replace('.', ''), 'right': game['O2'].replace('.', '')})
            except:
                pass
        
        return result

    def get_game_info(self, game_id):
        response = self.session.get(f'https://1xstavka.ru/en/statistic/game_popup/{game_id}/2/1/', timeout=10).text
        start = response.find('.setInitialData(') + 16
        for i in range(start, len(response)):
            if response[i] == '}' and response[i+1] == ')':
                return json.loads(response[start:i+1])

    def get_all(self):
        self.get_champs()

        for champ_id in self.champs:
            for item in self.get_games(champ_id):
                self.games.append({'id': item['id'], 'left': item['left'], 'right': item['right'], 'url': f'https://1xstavka.ru/en/line/Football/{champ_id}-{self.champs[champ_id]}/{item["id"]}-{item["left"]}-{item["right"]}/'.replace(' ', '-')})


    def get_cef(self, game_id):
        response = self.session.get(f'https://1xstavka.ru/LineFeed/GetGameZip?id={game_id}&lng=en&cfview=0&isSubGames=true&GroupEvents=true&allEventsGroupSubGames=true&countevents=250').json()
        for item in response['Value']['GE']:
            for i in item['E']:
                for k in i:
                    if k['T'] == 9 and k['P'] == 1:
                        return k['C']


def start():
    xbet_client = xbet()
    xbet_client.get_all()
    print('Спарсил игры, начинаю')

    current = []

    for match in xbet_client.games:
        try:
            if model.objects(check_id=match['id']):
                continue

            game = xbet_client.get_game_info(match['id'])

            if game.get('error'):
                continue

            if len(game['HeadToHead']['Games']) == 0:
                continue

            if game['DateStart'] > time.time() + 86400:
                continue


            team1 = game['Team1']['Title']
            team2 = game['Team2']['Title']

            total = (int(game['HeadToHead']['TeamStat1']['Goals']) + int(game['HeadToHead']['TeamStat2']['Goals'])) / len(game['HeadToHead']['Games'])
            score1 = []
            score2 = []


            for g in game['HeadToHead']['Team1Games']:
                if g['TeamTitle1'] == team1:
                    score1.append(g['Score1'])
                else:
                    score1.append(g['Score2'])

            for g in game['HeadToHead']['Team2Games']:
                if g['TeamTitle1'] == team2:
                    score2.append(g['Score1'])
                else:
                    score2.append(g['Score2'])

                    
            if score1.count(0) + score2.count(0) >= 5 and total > 3 and len(game['HeadToHead']['Games']) > 3:
                cef = xbet_client.get_cef(match['id'])
                if cef and cef >= 1.09:
                    print('Найден матч')

                    stat = f'https://1xstavka.ru/ru/statistic/game_popup/{match["id"]}/2/1/'
                    start = datetime.fromtimestamp(game['DateStart'])
                    model.create(
                        find_id=game['Id'],
                        check_id=match['id'],
                        url=match['url'],
                        info_url=stat,
                        teams=f'{team1} vs {team2}',
                        score1=','.join([str(item) for item in score1]),
                        score2=','.join([str(item) for item in score2]),
                        league=game['StageTitle'],
                        start=start,
                        h2h=len(game['HeadToHead']['Games']),
                        total=round(total, 2),
                        total_this=-1,
                        cef=cef
                    )
                    
                    params = {
                        'chat_id': -1001276138799,
                        'text': f'Подходящий матч!\n{team1} - {team2}\n{game["StageTitle"]}\nСтарт: {start.strftime("%Y-%m-%d %H:%M:%S")} по МСК\nСтавка: ТОТАЛ БОЛЬШЕ 1\nКоэфицент: {cef}\nСтатистика: {stat}\nСсылка: {match["url"]}'
                    }

                    requests.get('https://api.telegram.org/bot762286663:AAHVn7Vmigx3wTe2WYXvN9OsTG9Ju_tt_EI/sendMessage', params=params)

        except:
            logging.warning('Непредвиденная ошибка!')

def check():
    xbet_client = xbet()
    for item in model.objects(total_this=-1):
        data = xbet_client.get_game_info(item.check_id)

        if data.get('Score1') == None or data.get('Score2') == None:
            continue

        item.total_this = data['Score1'] + data['Score2']
        item.save()

while True:
    try:
        start()
        check()
    except:
        print('ОШИБКА')

    time.sleep(3600*3)

