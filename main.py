import requests
import time
from bs4 import BeautifulSoup as bs
import logging

handlers = [
        logging.FileHandler(f"{time.time()}.log"),
        logging.StreamHandler()
]


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s', handlers=handlers)

repeats = []
repeats2 = []

def check():
    session = requests.Session()
    matches = bs(session.get('https://ru.betsapi.com/ci/soccer', timeout=10).text, 'html.parser').find('div', {'class': 'table-responsive'}).findAll('tr')
    for match in matches:
        if match.attrs['id'] in repeats2:
            continue

        tds = match.findAll('td')

        time = tds[1].find("span").get_text().strip()
        if "+" in time:
            time = int(time.split('+')[0][:-10]) + int(time.split('+')[1])
        else:
            time = int(time[:-1])

        league = tds[0].find("a").get_text()

        left_team = tds[2].find("a").get_text()
        right_team = tds[4].find("a").get_text()

        score = tds[3].find("a").get_text().split('-')

        score = int(score[0]) + int(score[1])

        if score >= 3 and time < 80:

            history = tds[3].find("a").attrs['href']



            data = bs(session.get(f'https://ru.betsapi.com/rh/{history[3:]}', timeout=10).text, 'html.parser')

            cards = data.findAll('div', {'class': 'container'})[2].findAll('div', {'class': 'card'})
            
            score1 = 0
            volat1 = 0
            count1 = 0
            max1 = 0

            score2 = 0
            volat2 = 0
            count2 = 0
            max2 = 0

            score3 = 0
            volat3 = 0
            count3 = 0
            max3 = 0

            for index, card in enumerate(cards):
                if index == 0:
                    continue

                scores = []
                total_score = 0

                trs = card.find('table').findAll('tr')

                for tr in trs:
                    score_ = tr.findAll('td')[-1].find('a').get_text().split('-')
                    total_score += int(score_[0]) + int(score_[1])
                    scores.append(int(score_[0]) + int(score_[1]))
                
                total_score /= len(trs)
                volat = round(max(scores) - total_score, 2)
                if card.find('h3', {'class': 'card-title'}).get_text() == "Очные встречи":
                    score1 = round(total_score, 2)
                    volat1 = volat
                    count1 = len(trs)
                    max1 = max(scores)
                elif card.find('h3', {'class': 'card-title'}).get_text() == "Home History":
                    score2 = round(total_score, 2)
                    volat2 = volat
                    count2 = len(trs)
                    max2 = max(scores)
                elif card.find('h3', {'class': 'card-title'}).get_text() == "Away History":
                    score3 = round(total_score, 2)
                    volat3 = volat
                    count3 = len(trs)
                    max3 = max(scores)

            if volat1 > 2 or count1 < 3 or max3 - max1 > 1 or max2 - max1 > 1:
                repeats2.append(match.attrs['id'])
                continue
        
            if score >= score1 and volat1 <= 2 and volat2 <= 2 and volat3 <= 2:

                repeats2.append(match.attrs['id'])


                params = {
                    'chat_id': 409846254,
                    'text': f'Подходящий матч!\n{left_team} - {right_team}\nТекущий счет - {score}\nОчные встречи({count1}) средний счет - {score1}, V - {volat1}, MAX - {max1}\n{left_team}({count2}) cредний счет - {score2}, V - {volat2}, MAX - {max2}\n{right_team}({count3}) Средний счет - {score3}, V - {volat3}, MAX - {max3}\nТекущее время - {time}\nЛига - {league}\nСсылка - https://ru.betsapi.com/r/{history[3:]}'
                }

                requests.get('https://api.telegram.org/bot762286663:AAHVn7Vmigx3wTe2WYXvN9OsTG9Ju_tt_EI/sendMessage', params=params)

    
    session.close()



while True:

    try:
        check()
    except:
        logging.exception('Ошибка')
    time.sleep(300)